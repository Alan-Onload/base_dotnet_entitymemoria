using apimemory.Models;
using Microsoft.EntityFrameworkCore;

namespace apimemory.Database
{
    public class ProdutoContext:DbContext
    {
        public ProdutoContext(DbContextOptions<ProdutoContext> options):base(options)
        {

        }
        public DbSet<Produto> produto {get;set;} = null;
        
    }
}