using System.ComponentModel.DataAnnotations;
namespace apimemory.Models
{
    public class Produto
    {
        [Key]
        public int id { get; set; }
        public string data { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public string preco_unitario { get; set; }
        public string preco_venda { get; set; }
        public string fornecedor { get; set; }
    }
}