using apimemory.Database;
using apimemory.Models;
using Microsoft.AspNetCore.Mvc;

namespace apimemory.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController:ControllerBase
    {
        private readonly ProdutoContext db;
        public ProdutoController(ProdutoContext initdb){
            db = initdb;
        }

        [HttpGet]
        public List<Produto> listar()
        {
            var p = db.produto.ToList();
            return p;
            
        }

        [HttpGet("{id}")]
        public Produto pesquisar(int id)
        {
            var p = db.produto.Find(id);
            return p;
            
        }

        [HttpPost]
        public Produto salvar(Produto p)
        {
            p.data = DateTime.Now.ToString("dd/MM/yyyy");
            db.produto.Add(p);
            db.SaveChanges();
            return p;
            
        }

        [HttpPut("{id}")]
        public Produto alterar(int id,Produto produto)
        {
            var p = db.produto.Find(id);
            p.data = DateTime.Now.ToString("dd/MM/yyyy");
            p.nome = produto.nome;
            p.descricao = produto.descricao;
            p.preco_unitario = produto.preco_unitario;
            p.preco_venda = produto.preco_venda;
            db.SaveChanges();
            return p;
            
        }

        [HttpDelete("{id}")]
        public Produto deletar(int id)
        {
            var p = db.produto.Find(id);
            var produto = db.produto.Remove(p);
            db.SaveChanges();
            return p;
            
        }

        
    }
}